public class Computer {
    private String cpu = "";
    private int ram = 0;
    private String graphicsCard = "";
    private Boolean hasVideoCardCooler = null;
    private Boolean haveSsd = null;
    private int hddValue = 0;

    public Computer(String cpu, int ram, String graphicsCard) {
        this.cpu = cpu;
        this.ram = ram;
        this.graphicsCard = graphicsCard;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public String getGraphicsCard() {
        return graphicsCard;
    }

    public void setGraphicsCard(String graphicsCard) {
        this.graphicsCard = graphicsCard;
    }

    public Boolean getHasVideoCardCooler() {
        return hasVideoCardCooler;
    }

    public void setHasVideoCardCooler(Boolean hasVideoCardCooler) {
        this.hasVideoCardCooler = hasVideoCardCooler;
    }

    public Boolean getHaveSsd() {
        return haveSsd;
    }

    public void setHaveSsd(Boolean haveSsd) {
        this.haveSsd = haveSsd;
    }

    public int getHddValue() {
        return hddValue;
    }

    public void setHddValue(int hddValue) {
        this.hddValue = hddValue;
    }

}
