public class Country {
    private String country = "";
    private int populations = 0;
    private int square = 0;
    private String capital = "";
    private Boolean haveSea = null;

    public Country(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPopulations() {
        return populations;
    }

    public void setPopulations(int populations) {
        this.populations = populations;
    }

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public Boolean getHaveSea() {
        return haveSea;
    }

    public void setHaveSea(Boolean haveSea) {
        this.haveSea = haveSea;
    }
}
