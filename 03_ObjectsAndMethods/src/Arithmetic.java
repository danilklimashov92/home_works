
public class Arithmetic {

    public int numberOne = 0;
    public int numberTwo = 0;


    public Arithmetic(int numberOne, int numberTwo){
        this.numberOne = numberOne;
        this.numberTwo = numberTwo;

    }

    public static int summaNumbers(int numberOne, int numberTwo){
        return  numberOne + numberTwo;
    }

    public static int multiplicationNumbers(int numberOne, int numberTwo){
        return numberOne * numberTwo;
    }

    public static int maxNumber(int numberOne, int numberTwo){
        return  Math.max(numberOne, numberTwo);
    }

    public static int minNumber(int numberOne, int numberTwo){
        return Math.min(numberOne, numberTwo);

    }
}
