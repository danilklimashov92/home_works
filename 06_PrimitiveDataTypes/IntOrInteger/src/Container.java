public class Container {
    private Integer count = 1; // или изменить на примитив int

    public void addCount(int value) {
        count = count + value;
    }

    public int getCount() {
        return count;
    }
}
