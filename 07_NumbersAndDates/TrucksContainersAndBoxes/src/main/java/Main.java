import com.sun.source.util.SourcePositions;

import java.util.Scanner;

public class Main {

    private static final int BOXES_IN_CONTAINER = 27;
    private static final int BOXES_IN_MACHINE = 27 * 12;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String boxes = scanner.nextLine();

        // TODO: вывести в консоль коробки разложенные по грузовикам и контейнерам
        // пример вывода при вводе 2
        // для отступа используйте табуляцию - \t

        double box = Integer.valueOf(boxes);
        int container = 1;
        int machine = 1;

        if (box == 0){
            System.out.println("Необходимо:");
            System.out.println("грузовиков" + " - " + (machine - 1) + " шт.");
            System.out.println("контейнеров" + " - " + (container - 1) + " шт.");
        }

        else {
            System.out.println(" Грузовик: " + machine);
            System.out.println("\tКонтейнер: " + container);
            for (int i = 1; i <= box; i++) {
                System.out.println("\t\tЯщик: " + i);
                if(i == box){
                    break;
                }
                if (i % BOXES_IN_MACHINE == 0) {
                    machine++;
                    System.out.println("Грузовик: " + machine);
                }
                if (i % BOXES_IN_CONTAINER == 0) {
                    container++;
                    System.out.println("\tКонтейнер: " + container);
                }
            }
            System.out.println("Необходимо:");
            System.out.println("грузовиков" + " - " + machine + " шт.");
            System.out.println("контейнеров" + " - " + container + " шт.");
        }
    }
        /*
        Грузовик: 1
            Контейнер: 1
                Ящик: 1
                Ящик: 2
        Необходимо:
        грузовиков - 1 шт.
        контейнеров - 1 шт.
        */
}


