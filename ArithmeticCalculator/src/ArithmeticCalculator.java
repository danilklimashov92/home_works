public class ArithmeticCalculator {
    public int firstNumber;
    public int secondNumber;
    public int result;


    public ArithmeticCalculator(int firstNumber, int secondNumber) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    public int calculate(Operations value){
        switch (value) {
            case ADD:
                return firstNumber + secondNumber;

            case SUBTRACT:
                return firstNumber - secondNumber;

            case MULTIPLY:
                return firstNumber * secondNumber;

        }
        return result;
    }
}

