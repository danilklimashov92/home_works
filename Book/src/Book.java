public class Book {
    private final String bookTitle = "";
    private final String bookAuthor = "";
    private final int bookCountLetter = 0;
    private final int bookIsbn = 0;


    public String getBookTitle() {
        return bookTitle;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public int getBookCountLetter() {
        return bookCountLetter;
    }

    public int getBookIsbn() {
        return bookIsbn;
    }
}

