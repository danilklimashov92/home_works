public class Product {
    private final String name;
    private int price;
    private final int bareCode;

    public Product(String name, int bareCode) {
        this.name = name;
        this.bareCode = bareCode;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getBareCode() {
        return bareCode;
    }
}
