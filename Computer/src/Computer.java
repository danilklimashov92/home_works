public class Computer {
    private final String vendor;
    private final String name;
    private  Cpu cpu;
    private  Monitor monitor;
    private  Ram ram;
    private  Hdd hdd;
    private  Keyboard keyboard;

    public Computer(Hdd hdd, Monitor monitor, Keyboard keyboard, Ram ram, Cpu cpu, String vendor, String name){
        this.name = name;
        this.vendor = vendor;
        this.cpu = cpu;
        this.monitor = monitor;
        this.ram = ram;
        this.hdd = hdd;
        this.keyboard = keyboard;
    }
    public double totalWeight(){
        return cpu.getWeight() + keyboard.getWeight() + ram.getWeight() +
                hdd.getWeight()+ monitor.getWeight();
    }

    public String getVendor() {
        return vendor;
    }

    public String getName() {
        return name;
    }

    public Cpu getCpu() {
        return cpu;
    }

    public void setCpu(Cpu cpu) {
        this.cpu = cpu;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    public Ram getRam() {
        return ram;
    }

    public void setRam(Ram ram) {
        this.ram = ram;
    }

    public Hdd getHdd() {
        return hdd;
    }

    public void setHdd(Hdd hdd) {
        this.hdd = hdd;
    }

    public Keyboard getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(Keyboard keyboard) {
        this.keyboard = keyboard;
    }

    public String toString(){
        return "Характеристики компьютера :\n" +
        "Производитель :" + getVendor() + "\n" +

        "Название компьютера :" + getName() + "\n" +

        "Общий вес деталей :" + totalWeight() + "\n" +

        "Процессор - " + getCpu() + "\n" +
            "Параметры процессора: \n"+
            "частота процессора :" + cpu.getFrequency() + "\n" +
            "Количество ядер :" + cpu.getNumberOfCores() + "\n" +
            "Производитель :" + cpu.getManufacturer() + "\n" +
            "Вес :" + cpu.getWeight() + "\n" +

        "Жесткий диск - " + getHdd() + "\n" +
            "Параметры жесткого диска :" + "\n" +
            "Тип жесткого диска :" + hdd.getHddType() + "\n" +
            "Объем памяти :" + hdd.getValue() + "\n" +
            "Вес :" + hdd.getWeight() + "\n" +

        "Оперативная память - " + getRam() + "\n" +
            "Параметры оперативной памяти :" + "\n" +
            "Тип оперативной памяти :" + ram.getType() + "\n" +
            "Объем оперативной памяти :" + ram.getValue() + "\n" +
            "Вес :" + ram.getWeight() + "\n" +

        "Монитор - " + getMonitor() + "\n" +
            "Параметры монитора :" + "\n" +
            "Диагональ :" + monitor.getDiagonal() + "\n" +
            "Тип матрицы :" + monitor.getTypeMonitor() + "\n" +
            "Вес :" + monitor.getWeight() + "\n" +

        "Клавитура - " + getKeyboard() + "\n" +
            "Параметры клавиатуры :" + "\n" +
            "Тип клавиатуры :" + keyboard.getTypeKeyboard() + "\n" +
            "Есть подсветка : " + keyboard.getpresenceOfBacklight() + "\n" +
            "Вес :" + keyboard.getWeight();
    }

}

