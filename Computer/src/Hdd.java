public class Hdd {
    private HddType hddType;
    private final int value;
    private final double weight;

    public Hdd (int value, double weight) {
        this.value = value;
        this.weight = weight;
    }

    public HddType getHddType() {
        return hddType;
    }

    public int getValue() {
        return value;
    }

    public double getWeight() {
        return weight;
    }
}
