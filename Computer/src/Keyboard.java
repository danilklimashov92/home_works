public class Keyboard {
    private final String typeKeyboard;
    private final boolean presenceOfBacklight;
    private final double weight;

    public Keyboard(String typeKeyboard, boolean presenceOfBacklight, double weight) {
        this.typeKeyboard = typeKeyboard;
        this.presenceOfBacklight = presenceOfBacklight;
        this.weight = weight;
    }

    public String getTypeKeyboard() {
        return typeKeyboard;
    }

    public boolean getpresenceOfBacklight() {
        return presenceOfBacklight;
    }

    public double getWeight() {
        return weight;
    }
}
