public class Monitor {
    private TypeMonitor typeMonitor;
    private final double diagonal;
    private final double weight;

    public Monitor(double diagonal, double weight) {
        this.diagonal = diagonal;
        this.weight = weight;
    }

    public TypeMonitor getTypeMonitor() {
        return  typeMonitor;
    }

    public double getDiagonal() {
        return diagonal;
    }

    public double getWeight() {
        return weight;
    }
}
