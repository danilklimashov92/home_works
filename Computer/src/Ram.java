public class Ram {
    private final String type;
    private final int value;
    private final double weight;

    public Ram(String type, int value, double weight) {
        this.type = type;
        this.value = value;
        this.weight = weight;
    }

    public String getType() {
        return type;
    }

    public int getValue() {
        return value;
    }

    public double getWeight() {
        return weight;
    }
}
