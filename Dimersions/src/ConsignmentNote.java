public class Consignment_note {
    private final int weight;
    private final String deliveryArdress;
    private final Boolean canBeTurnedOver ;
    private final String registrationNumber;
    private final Boolean isFragile;

    public Consignment_note(int weight, String deliveryArdress, Boolean canBeTurnedOver, String registrationNumber, Boolean isFragile) {
        this.weight = weight;
        this.deliveryArdress = deliveryArdress;
        this.canBeTurnedOver = canBeTurnedOver;
        this.registrationNumber = registrationNumber;
        this.isFragile = isFragile;
    }

    public Consignment_note setWeight(int weight) {
        return new Consignment_note(weight, deliveryArdress, canBeTurnedOver, registrationNumber, isFragile);
    }

    public Consignment_note setDeliveryArdress(String deliveryArdress) {
        return new Consignment_note(weight, deliveryArdress, canBeTurnedOver, registrationNumber, isFragile);
    }

    public Consignment_note setCanBeTurnedOver(Boolean canBeTurnedOver) {
        return new Consignment_note(weight, deliveryArdress, canBeTurnedOver, registrationNumber, isFragile);
    }

    public Consignment_note setRegistrationNumber(String registrationNumber) {
        return new Consignment_note(weight, deliveryArdress, canBeTurnedOver, registrationNumber, isFragile);
    }

    public Consignment_note setFragile(Boolean isFragile) {
        return new Consignment_note(weight, deliveryArdress, canBeTurnedOver, registrationNumber, isFragile);
    }
}

