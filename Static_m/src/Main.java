public class Main {

    public static void main(String[] args) {
        Basket andreyBasket = new Basket();
        andreyBasket.add("Milk", 40);
        andreyBasket.add("Cheese", 60);
        andreyBasket.print("Basket 1");

        Basket antonBasket = new Basket();
        antonBasket.add("Pine", 100 , 3, 1.5);
        antonBasket.print("Anton Basket");

        Printer textPrinter = new Printer();
        textPrinter.add("");
        textPrinter.print();

    }
}
