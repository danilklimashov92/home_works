public class Printer {
    private String queue = "";
    private String title = "";
    private int pages = 0;
    private int totalPages = 0;

    public Printer(){
        queue = "Добавленно страниц для печати:";
    }

  /*  public Printer(String title, String text, int countLetters){
        this();
        queue += text;
        this.title = title;
        this.countLetters = countLetters;

    }*/

    public void add(String text){ add("", text);}

    public void add(String title, String text){add(title, text, 1);}

    public void add(String title, String text, int pages){

        queue = queue +"\n" + title + "\n" + text + " " + "\n" +pages;
        this.pages += pages;
        this.totalPages += pages;
    }

    public void clear(){
        queue = "";
        pages = 0;
        title = "";
    }

    public int getPendingPagesCount(){ return totalPages;}

    public String getTitle(){ return title;}

    public void print(){
        System.out.println(title);
        if(queue.isEmpty()){
            System.out.println("Страниц на печать нет");
        }
        else {
            System.out.println(queue);
        }
    }

}
